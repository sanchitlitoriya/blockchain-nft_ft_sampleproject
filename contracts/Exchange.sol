//SPDX-License-Identifier: Unlicense
pragma solidity 0.8.1;

import "hardhat/console.sol";
import "@openzeppelin/contracts-upgradeable/token/ERC1155/IERC1155Upgradeable.sol";
import "@openzeppelin/contracts-upgradeable/proxy/utils/Initializable.sol";

/// @title Shop contract
/// @author Sanchit Litoriya
/// @notice You can use this to buy FRC and TRC FTs in exchange of ether
contract Exchange is Initializable {
  address private _mintOwner ;
   uint256 private constant _FRC = 5;
   uint256 private constant _TRC = 10; 
   IERC1155Upgradeable private _currencyStore;

   /// @notice initializer
   /// @param mintOwner_ address of the account from which Currency was deployed
   /// @param currencyStore_ address of Currency contract
   function init(address mintOwner_, address currencyStore_) public initializer() {
      _mintOwner = mintOwner_;
       _currencyStore = IERC1155Upgradeable(currencyStore_);
   }
    
   /// @notice buy FRC and TRC tokens in exchange for ether and any remaining ether will be transfered back to buyer
   /// @param _QtyFRC quantity of FRC tokens to be purchased
   /// @param _QtyTrc quantity of TRC tokens to be purchased
   function buy( uint256 _QtyFRC, uint256 _QtyTRC) external payable {
       
        uint256 _amountRequired = ( _TRC *_QtyTRC + _FRC *_QtyFRC) ;
        console.log(_amountRequired);
        address payable _buyer = payable(msg.sender);
        
        require(msg.value >= _amountRequired, "Exchange: insufficient funds to buy coins");

        _currencyStore.safeTransferFrom(_mintOwner, _buyer, _FRC, _QtyFRC, "");
        _currencyStore.safeTransferFrom(_mintOwner, _buyer, _TRC, _QtyTRC, "");

        if (msg.value > _amountRequired){
            uint256 _remainder = msg.value - _amountRequired;
        _buyer.transfer(_remainder);
        }
    }
    
   fallback() external payable {
       
   }
   
   receive() external payable {
       
   }
}