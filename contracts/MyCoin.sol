//SPDX-License-Identifier: Unlicense
pragma solidity 0.8.1;

import "hardhat/console.sol";
import "@openzeppelin/contracts-upgradeable/token/ERC1155/IERC1155Upgradeable.sol";
import "@openzeppelin/contracts-upgradeable/token/ERC721/ERC721Upgradeable.sol";

/// @title NFT minter 
/// @author Sanchit Litoriya
/// @notice You can use this to mint NFT in exchange for FTs(TRC and FRC)
contract MyCoin is ERC721Upgradeable {
    IERC1155Upgradeable private _currency;
    address private _owner;

    /// @notice initializer - will set owner of this contract and get instance of passed address contract i.e. currency contract(FT minter)
    /// @param currency_ address of FTs(TRC and FRC) minter contarct  
    function init(address currency_) public initializer(){
        _currency = IERC1155Upgradeable(currency_);
        _owner = msg.sender;
    }
    
    event tokenCreated(uint256 _tokenName);

    /// @notice mint NFT and emit event tokenCreated with NFTs name
    /// @param _QtyFRC quantity for FRC to be part of NFT
    /// @param _QtyTRC quantity of TRC to be part of  NFT
    function buy( uint256 _QtyFRC, uint256 _QtyTRC) public returns(uint256){
        address _buyer = msg.sender;
        
        _currency.safeTransferFrom(_buyer, _owner, 5, _QtyFRC, "");
        _currency.safeTransferFrom(_buyer, _owner, 10, _QtyTRC, "");
        
        uint256 _tokenName = 10; 
        uint256 _temp = _QtyTRC;
        
        while( _temp > 0) {
            _temp /= 10;
            _tokenName *= 10;
        }
        
        _tokenName = _tokenName * _QtyFRC * 10 + _QtyTRC * 10 + 1;

        _safeMint(_buyer, _tokenName);
        emit tokenCreated(_tokenName);
        return _tokenName;
        
    }
}