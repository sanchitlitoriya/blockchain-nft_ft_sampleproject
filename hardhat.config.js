require("@nomiclabs/hardhat-waffle");
require("hardhat-deploy");
// let secret = require('./secret.json');
require('dotenv').config();
require("@nomiclabs/hardhat-ethers");
require("@nomiclabs/hardhat-etherscan");

// This is a sample Hardhat task. To learn how to create your own go to
// https://hardhat.org/guides/create-task.html
task("accounts", "Prints the list of accounts", async (taskArgs, hre) => {
  const accounts = await hre.ethers.getSigners();

  for (const account of accounts) {
    console.log(account.address);
  }
});

// You need to export an object to set up your config
// Go to https://hardhat.org/config/ to learn more

/**
 * @type import('hardhat/config').HardhatUserConfig
 */
module.exports = {
  solidity: "0.8.1",
  namedAccounts: {
    deployer: 0,
  },
  networks: {
    ropsten : {
      url: process.env.URL,
      accounts: [`0x${process.env.KEY}`]
    }
  },
  etherscan: {
    apiKey: process.env.MYCOIN,
  }
};
