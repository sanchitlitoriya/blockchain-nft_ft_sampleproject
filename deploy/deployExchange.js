module.exports = async ({getNamedAccounts, deployments, ethers}) => {
    const {deploy} = deployments;
    const {deployer} = await getNamedAccounts();
    // console.log(deployer);
    // console.log(deployer);
    const currency = await ethers.getContract('Currency');//deployments.get('Currency');

    // console.log(deployer , currency.address);
    const exchange = await deploy('Exchange', {
        from: deployer,
        // args:[deployer, currency.address],
        log: true,
        proxy: {
            owner: deployer,
            execute: {
                methodName: "init",
                args: [deployer, currency.address],
              }
            // methodName: 'init',
            // args:[deployer, currency.address],
          }
      });
    //   console.log(currency);
    await currency.setApprovalForAll(exchange.address, true);
    


  };
  module.exports.tags = ['Exchange'];
  module.exports.dependencies = ['Currency'];