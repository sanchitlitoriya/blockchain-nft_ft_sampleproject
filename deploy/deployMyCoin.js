module.exports = async ({getNamedAccounts, deployments, ethers}) => {
    const {deploy} = deployments;
    const {deployer} = await getNamedAccounts();
    // console.log(deployer);
    // console.log(deployer);
    const currency = await ethers.getContract('Currency');

    // console.log(deployer , currency.address);
    const myCoin = await deploy('MyCoin', {
        from: deployer,
        log: true,
        proxy: {
            owner: deployer,
            execute: {
                methodName: 'init',
                args:[currency.address],
            }
          }
      });
    
    await currency.setApprovalForAll(myCoin.address, true);

  };
  module.exports.tags = ['MyCoin'];
  module.exports.dependencies = ['Currency', 'Exchange'];