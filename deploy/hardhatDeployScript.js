module.exports = async ({getNamedAccounts, deployments}) => {
    const {deploy} = deployments;
    const {deployer} = await getNamedAccounts();
    console.log(deployer);
    await deploy('Currency', {
      from: deployer,
      log: true,
      proxy:  {
        owner: deployer,
        methodName: 'init',
      }
    });
  };
  module.exports.tags = ['Currency'];