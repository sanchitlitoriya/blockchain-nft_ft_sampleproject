# Basic Sample FT, NFT Hardhat Project (used ERC1155, ERC721)

<h4>Part 1</h4>
Create and deploy an ERC1155 Contract
Use open-zeppelin libraries

<ul>
<li>Initialize the contract with two types of FTs
<ul>
<li>5 Rupee Coin (FRC)</li>
<li>10 Rupee Note (TRC)</li>
</ul>
</li>
<li>Create a Shop Contract for buying the "FRC" and "TRC" tokens.</li>
<li>
Create a buy method which receives ETH and calculates and transfers the maximum possible tokens at that price</li>
<li>The buy method must return the remainder amount if any</li>
<li>The user should be allowed to buy the type of token they want</li>
</ul>

<h4>Part 2</h4>
<ul>
<li>Create and deploy NFT Contract which accepts any amount of our tokens(FRC and TRC)</li>
<li>10 Rupee Note (TRC)</li>
<li>Create a Shop Contract for buying the "FRC" and "TRC" tokens.</li>
<li>
The NFT Contract mints a Token to the buyer, where token id =  amount of FRC + "FRC" +  amount of TRC + "TRC"; <br/> e.g. if user sends 5 FRC and 3 TRC then token ID will be - "FRC5TRC3"(used => "FRC" = 0 "TRC" = 1; 2FRC5TRC => 2051)</li>
<li>The contract rejects the transaction if the Token of the given combination has already been minted</li>
</ul>

<h6> Note: The contracts should be upgradeable, deployed to testnet and verified </h6>

<h2>Address of deployed contracts proxy</h2>
<ol>
<li><b>Currency : </b>0x1a77f5DD74D936a15367D9bd9304D80FDa6C6c85</li>
<li><b>Exchange : </b>0x71fb238e234Bcccb9568e99735Fa376271D8799E</li>
<li><b>MyCoin : </b>0x3034C9D1aB13E8acCeC6Ca797f8ef3732cBd0311</li>
</ol>